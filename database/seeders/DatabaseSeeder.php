<?php

namespace Database\Seeders;

use App\Models\Jabatan;
use App\Models\Profile;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Jabatan::create([
            'status' => 'tamu'
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
