<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CategoriesPertanyaanController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\UnansweredController;
use App\Http\Controllers\PopularController;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
// use App\Http\Controllers\NisController;
use App\Http\Controllers\RegisterController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Http\Controllers\JabatanController;
use App\Http\Controllers\ProfileController;

Route::resource('jabatan', JabatanController::class)->middleware('admin');
Route::resource('profile', ProfileController::class)->only('index', 'update', 'edit', 'show');
















use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\KomentarController;

Route::resource('pertanyaan', PertanyaanController::class);
Route::resource('categories', KategoriController::class)->only([
    'index','edit','destroy','store','update'
])->middleware('admin');
Route::get('kategori/{categories}', [KategoriController::class,'show']);
Route::get('categories/create', [KategoriController::class,'create']);



Route::post('komentar/{id}', [KomentarController::class, 'store']);
Route::put('komentar/{id}', [KomentarController::class, 'update']);
Route::delete('komentar/{id}', [KomentarController::class, 'destroy']);



Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);
Route::post('/logout', [LoginController::class, 'logout'])->middleware('auth');

// Route::get('/nis/checkNis', [NisController::class, 'checkNis'])->middleware('auth');

Route::get('/', function () {
    return redirect('/pertanyaan');
})->middleware('auth');

Route::get('/unanswered', [UnansweredController::class, 'index']);
Route::get('/popular', [PopularController::class, 'index']);


