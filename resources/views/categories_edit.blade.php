@extends('halamantanpaforum')
@section('title')
    Edit Kategori
@endsection
@section('isihalamankosong')

<form method="POST" action="/categories/{{$category->id}}">
    @method('PUT')
    @csrf
    <div class="form-group">
        <label for="nama">Edit Kategori / Tema</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"
            placeholder="Input nama kategori" name="nama" required autofocus
            value="{{ old('nama', $category->nama) }}">
        @error('nama')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
    <a href="/categories" class="btn btn-danger">Batalkan Edit</a>

</form>

@endsection
