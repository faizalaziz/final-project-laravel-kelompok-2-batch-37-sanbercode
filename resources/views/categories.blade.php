@extends('halamantanpaforum')
@section('title')
    Daftar Kategori
@endsection
@push('scriptatas')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">    
@endpush
@section('isihalamankosong')

<div class="card card-primary card-outline">
    <div class="float-left card-header">
        <a href="/pertanyaan" class="btn btn-primary">< Kembali</a>
    </div>
    <div class="card-header">
        <h3 class="card-title"><b>Kategori / Tema</b></h3>
    </div>
    @auth
    <a href="/categories/create" class="btn btn-info btn-sm my-3">Tambah Kategori</a>
    @endauth

    
    <div class="table">
        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                Kategori berhasil ditambahkan
            </div>
        @endif
        @if (session()->has('deleteSuccess'))
            <div class="alert alert-danger" role="alert">
                Kategori berhasil dihapus
            </div>
        @endif

        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Kategori</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $category->nama }}</td>
                        <td>
                            {{-- <a href="/categories/{{ $category->id }}" class="badge bg-info">
                                <i class="bi bi-eye"></i>
                            </a> --}}
                            <a href="/categories/{{ $category->id }}/edit" class="badge bg-warning">
                                {{-- <span data-feather="edit"></span> --}}
                                <i class="bi bi-pencil-square"></i>
                            </a>
                            <form action="/categories/{{ $category->id }}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="badge bg-danger border-0" onclick="return confirm('Are You sure')">
                                    {{-- <span data-feather="x-circle"></span> --}}
                                    <i class="bi bi-x-lg"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</main>
@endsection
