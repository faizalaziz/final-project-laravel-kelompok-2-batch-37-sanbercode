<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/sign-in/" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {{-- <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet" /> --}}

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/signin.css') }}" rel="stylesheet" />
</head>

<body class="text-center">
    <form class="form-signin" action="/login" method="POST">
        @if (session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session()->has('loginError'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('loginError') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @csrf
        <h1 class="h3 font-weight-normal">Forum</h1>
        <h1 class="h3 mb-5 mt-2 font-weight-normal">SMA CONTOH</h1>
        <h1 class="h3 mb-5 font-weight-normal">--Please sign in--</h1>
        <label for="email" class="sr-only">Email address</label>
        <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror"
            placeholder="Email address" required autofocus />
        @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
        <label for="password" class="sr-only ">Password</label>
        <input type="password" name="password" id="password"
            class="form-control @error('password') is-invalid @enderror" placeholder="Password" required />
        @error('password')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
        <button class="btn btn-lg btn-primary btn-block" type="submit">
            Sign in
        </button>
        <small class="d-block text-center my-3">
            Not Registered? <a href="/register">Register Now</a>
        </small>
        <small class="d-block text-center my-3">
            Langsung menuju forum tanpa login <a href="/pertanyaan">Klik Disini!</a>
        </small>

    </form>
</body>

</html>
