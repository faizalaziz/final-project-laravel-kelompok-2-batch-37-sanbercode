@extends('halamantanpaforum')
@section('title')
    Tambah Kategori Baru
@endsection
@section('isihalamankosong')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h1 class="h2">Buat Kategori</h1>
    </div>
    <div class="col-lg-8">
        <form method="POST" action="/categories" class="mb-5">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Kategori</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"
                    placeholder="Input nama" name="nama" required autofocus value="{{ old('nama') }}">
                @error('nama')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Create</button>
            <a href="/categories" class="btn btn-danger">Batal Membuat Kategori</a>
        </form>
    </div>
</main>
@endsection
