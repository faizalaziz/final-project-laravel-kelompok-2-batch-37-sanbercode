<div class="wrapper">

    <div class="content-wrapper mt-1 p-3">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-3">
                    {{-- @auth
            <a href="/pertanyaan/create" class="btn btn-primary btn-block mb-3">Buat Pertanyaan</a>
            @endauth
  
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Kategori</h3>
  
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
            </div> --}}

                    <div class="card-body p-0">
                        <ul class="nav nav-pills flex-column">
                            @yield('kategori')
                        </ul>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

            <div class="col-sm">
                @yield('createpertanyaan')
                @yield('isihalamankosong')
            </div>
            <!-- /.col -->
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Indonesia</b>
    </div>
    <strong>Copyright &copy; 2022 <strong>SMA CONTOH</strong> All rights reserved.
</footer>


</div>
