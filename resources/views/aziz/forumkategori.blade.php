@extends('selamatdatang')
@section('title')
    Forum | {{$kategori->nama}}
@endsection
@section('forum')

<div class="card card-primary card-outline">
    <!-- /.card-header -->
    <div class="card-header">
        <h3 class="card-title">Forum</h3>
    
        <div class="card-tools">
          <div class="input-group input-group-sm">
            <input type="text" class="form-control" placeholder="Cari Judul Pertanyaan" id="searchforum" onkeyup="myFunction()">
            <div class="input-group-append">
              <div class="btn btn-primary">
                <i class="fas fa-search"></i>
              </div>
            </div>
          </div>
        </div>
    </div>
            <div class="card-body p-0">
                <div class="table-responsive mailbox-messages p-3">
                <table class="table table-hover table-striped" id="myTable">
                  <thead class="d-none">
                    <th>Judul</th>
                    <th>Penulis</th>
                    <th>kategori</th>
                    <th>komentar</th>

                  </thead>
                <tbody>
                @forelse ($kategori->pertanyaan->sortByDesc('created_at') as $satupertanyaan)
                  <tr> 
                      <td class="mailbox-subject" width="40%"><a href="/pertanyaan/{{$satupertanyaan->id}}">
                        @if (strlen($satupertanyaan->deskripsi) < 60)
                            {{strip_tags($satupertanyaan->deskripsi)}}
                        @else
                            {{strip_tags($satupertanyaan->deskripsi). "..."}}
                        @endif
                        
                        </a></td>
                      <td class="mailbox-name" width="30%"><a href="/profile/{{$satupertanyaan->user->id}}">{{$satupertanyaan->user->username}}</a>
                      | 
                      @if (!empty($satupertanyaan->user->profile->jabatan->status))
                          {{$satupertanyaan->user->profile->jabatan->status}}
                      @else
                          -
                      @endif
                      </td>
                      <td class="mailbox-name" width="20%">{{$satupertanyaan->kategoris->nama}}</td>
                      <td class="mailbox-name " width = "10%"><i class="fas fa-comment mr-2"></i>{{count($satupertanyaan->komentar)}}</td>
                      
                  </tr>
                @empty
                  <tr>
                    <td>Tidak ada Pertanyaan</td>
                  </tr>
                @endforelse
                </tbody>
                </table>

            </div>
    </div>
</div>

    
@endsection

@section('kategori')
@foreach ($kategori_samping as $satukategori)
  <li class="nav-item">
    <a href="/kategori/{{$satukategori->id}}" class="nav-link">
      {{$satukategori->nama}}
    </a>
  </li>
@endforeach

@endsection