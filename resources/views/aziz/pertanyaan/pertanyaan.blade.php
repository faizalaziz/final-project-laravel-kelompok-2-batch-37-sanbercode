@extends('selamatdatang')
@section('title')
    Forum
@endsection
@section('forum')

<div class="card card-primary card-outline">
    <!-- /.card-header -->
            <div class="card-header">
                <h3 class="card-title">Forum</h3>
            
                <div class="card-tools">
                  <div class="input-group input-group-sm">
                    <input type="text" class="form-control" placeholder="Cari Judul Pertanyaan" id="searchforum" onkeyup="myFunction()">
                    <div class="input-group-append">
                      <div class="btn btn-primary">
                        <i class="fas fa-search"></i>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive mailbox-messages p-3">
                <table class="table table-hover table-striped" id="myTable">
                <thead class="d-none">
                  <th class="w-50">Judul</th>
                  <th class="w-30">Penulis</th>
                  <th class="w-20">kategori</th>
                  <th class="w-20">komentar</th>

                </thead>
                <tbody>
                @forelse ($kumpulanpertanyaan as $pertanyaan)
                  <tr> 
                      <td class="mailbox-subject" width="40%"><a href="/pertanyaan/{{$pertanyaan->id}}">
                        @if (strlen($pertanyaan->deskripsi) < 60)
                            {{strip_tags($pertanyaan->deskripsi)}}
                        @else
                            {{strip_tags($pertanyaan->deskripsi). "..."}}
                        @endif
                        
                        </a></td>
                      {{-- <td class="mailbox-date">5 mins ago</td> --}}
                      <td class="mailbox-name " width = "30%"><a href="/profile/{{$pertanyaan->user->id}}">{{$pertanyaan->user->username}}</a> | 
                        @if (!empty($pertanyaan->user->profile->jabatan->status))
                          {{$pertanyaan->user->profile->jabatan->status}}
                        @else
                          -
                        @endif
                      </td>
                      <td class="mailbox-name " width = "20%">{{$pertanyaan->kategoris->nama}}</td>
                      <td class="mailbox-name " width = "10%"><i class="fas fa-comment mr-2"></i>{{count($pertanyaan->komentar)}}</td>
                      
                  </tr>
                @empty
                  <tr>
                    <td>Tidak ada Pertanyaan</td>
                  </tr>
                @endforelse
                </tbody>
                </table>

            </div>
    </div>
</div>

    
@endsection

@section('kategori')
@foreach ($kategori as $satukategori)
  <li class="nav-item">
    <a href="/kategori/{{$satukategori->id}}" class="nav-link">
      {{$satukategori->nama}}
    </a>
  </li>
@endforeach

@endsection
@push('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if(Session::has('success'))
  <script type="text/javascript">

  function message() {
    Swal.fire({
      title: "Berhasil!",
      text: "Pertanyaan/Diskusi telah dibuat!",
      icon: "success",
      confirmButtonText: "Cool",
  }) };

  window.onload = message;
 </script>
@endif
@endpush
