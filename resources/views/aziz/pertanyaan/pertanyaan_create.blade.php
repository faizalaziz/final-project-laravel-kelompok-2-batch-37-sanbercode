@extends('selamatdatang')
@section('title')
    Buat Pertanyaan
@endsection
@section('forum')
    <div class="card card-primary card-outline">
        <!-- /.card-header -->
        <div class="float-left card-header">
            <a href="/pertanyaan" class="btn btn-primary">< Kembali</a>
        </div>
        <div class="card-header">
            <h3 class="card-title">Buat Pertanyaan Baru</h3>
        </div>
         
        <!-- /.card-body -->
        <form action="/pertanyaan" method="post" enctype="multipart/form-data" >
        <div class="card-body">
                @csrf  
                <div class="form-group">
                    <label for="kategori_id">Pilih kategori</label>
                    <select name="kategori_id" id="kategori_id" class="form-control">
                        @foreach ($kategori as $satukategori)
                            <option value="{{$satukategori->id}}">{{$satukategori->nama}}</option>
                        @endforeach
                    </select>
                    @error('kategori_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="deskripsi">Tulis pertanyaanmu!</label>
                    <textarea id="deskripsi" name="deskripsi" class="form-control" style="height: 300px"></textarea>
                    @error('deskripsi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                
                </div>

                <div class="form-group">
                    <label for="gambar"> <span style="color :blue">*</span>Gambar :</label>
                    <input type="file" name="gambar" id="gambar" class="form-control">
                    <p class="help-block">Max. 32MB</p>
                    <label  class="help-block"> <span style="color :blue">*</span>Optional</label>
                </div>

        </div>
         
        <!-- /.card-footer -->
        <div class="card-footer">
            <div class="float-right">
            <button type="submit" class="btn btn-primary">Buat Pertanyaan</button>
            </div>
        </div>
        </form>
      
    </div>
    <!-- /.card -->

@endsection


@section('kategori')
@foreach ($kategori as $satukategori)
  <li class="nav-item">
    <a href="/categories/{{$satukategori->id}}" class="nav-link">
      {{$satukategori->nama}}
    </a>
  </li>
@endforeach

@endsection

@push('scriptatas')
<script src="https://cdn.tiny.cloud/1/g4fsy22erv44flvoxov67ikjdrtlpvfo6gw0yffuunsg9vp7/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: '#deskripsi',
        menubar:false,
        statusbar: false,
    });
  </script>
@endpush
@push('scripts')

@endpush