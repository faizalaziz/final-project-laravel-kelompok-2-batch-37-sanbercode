@extends('halamantanpaforum')
@section('title')
    Detail Diskusi
@endsection
@section('isihalamankosong')
<div class="card card-primary card-outline">
  {{-- SHOW PERTANYAAN --}}
    <div class="card-header">
      <a class="btn btn-primary card-title" href="/pertanyaan"><i class="fas fa-chevron-left"></i>Kembali</a>
      @if ($pertanyaan_id->user_id === Auth::id())
          <div class="card-tools">
            <a class="btn btn-primary card-title" href="/pertanyaan/{{$pertanyaan_id->id}}/edit">Edit</a>
            <form action="/pertanyaan/{{$pertanyaan_id->id}}" method="post" style="display:inline-block">
              @csrf
              @method('DELETE')
              <input type="submit" value="Hapus" class="btn btn-danger delete-confirm" name="delete" id="delete"/>
            </form>
          </div>
      @endif
      
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
      <div class="mailbox-read-info m-2">
        <h1 class="text-justify">{!!$pertanyaan_id->deskripsi!!}</h1>
        <p>Oleh <b><a href="/profile/{{$pertanyaan_id->user->profile->id}}" style="color:black"> {{$pertanyaan_id->user->username}} | 
        @if (!empty($pertanyaan_id->user->profile->jabatan->status))
          {{$pertanyaan_id->user->profile->jabatan->status}}
        @else
          -    
        @endif
        </a></b> di kategori <a href="/kategori/{{$pertanyaan_id->kategoris->id}}">{{$pertanyaan_id->kategoris->nama}}</a></p>
        @if (empty($pertanyaan_id->gambar))
            
        @else
          <img src="{{asset('/uploads/'. $pertanyaan_id->gambar)}}" class="w-50 img-fluid mx-auto d-block " alt="Gambar">
        @endif
      </div>
    </div>

    @auth

    {{-- CREATE KOMENTAR --}}
    <div class="card-header my-2" >
      <form action="/komentar/{{$pertanyaan_id->id}}" method="post" class="form">
        @csrf
        <textarea name="komentar" id="komentar" cols="30" rows="5" placeholder="Tulis Komentar" class="form-control"></textarea>  
        <div class="float-right">
          <input type="submit" class="btn btn-success" value="Comment">
        </div>
      </form>
    </div>
            
    @endauth

</div>

{{-- READ KOMENTAR --}}
@forelse ($pertanyaan_id->komentar as $satukomentar)
    <div class="card card-outline" id="komentar_{{$satukomentar->id}}">
      <div class="card-footer my-2">
          <p><a class="text-dark" href="/profile/{{$satukomentar->user->id}}"><b>{{$satukomentar->user->username}}</b></a>
          |
          @if (!empty($satukomentar->user->profile->jabatan->status))
              {{$satukomentar->user->profile->jabatan->status}}
          @else
            -
          @endif
          </p>
          <p class="text-justify">{{$satukomentar->komentar}}</p>
          @if ($satukomentar->user->id === Auth::id())
          <div class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#edit-{{$satukomentar->id}}">Edit</button>
            <form action="/komentar/{{$satukomentar->id}}" style="display: inline-block" method="post">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger delete-confirm-komentar" >Hapus</button>
            </form>
          </div>

          <!-- Modal -->
          <div class="modal fade" id="edit-{{$satukomentar->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Edit Komentar</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="/komentar/{{$satukomentar->id}}" method="post" class="form">
                    @csrf
                    @method('PUT')
                    <textarea name="komentar_edit" id="komentar_edit" cols="30" rows="5" placeholder="Edit Komentar" class="form-control">{{$satukomentar->komentar}}</textarea>  
                    <div class="float-right">
                      <input type="submit" class="btn btn-success" value="Edit">
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          @endif
      </div>
    </div>
@empty
    <div class="card card-outline">
      <div class="card-footer my-2">
        <p class="text-center">Tidak ada Komentar</p>
      </div>
    </div>
@endforelse

    






    {{-- CRUD KOMENTAR --}}
    
  <!-- /.card -->
@endsection
@push('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script>
$('.delete-confirm').click(function(event) {
     var form =  $(this).closest("form");
     var name = $(this).data("name");
     event.preventDefault();
     swal({
         title: `Apakah anda yakin akan menghapus Pertanyaan ini?`,
         text: "Jika kamu menghapus Pertanyaan ini, datanya akan hilang selamanya",
         icon: "warning",
         buttons: true,
         dangerMode: true,
     })
     .then((willDelete) => {
       if (willDelete) {
         form.submit();
       }
     });
 });

 $('.delete-confirm-komentar').click(function(event) {
     var form =  $(this).closest("form");
     var name = $(this).data("name");
     event.preventDefault();
     swal({
         title: `Apakah anda yakin akan menghapus komentar ini?`,
         text: "Jika kamu menghapus komentar ini, komentarnya akan hilang selamanya",
         icon: "warning",
         buttons: true,
         dangerMode: true,
     })
     .then((willDelete) => {
       if (willDelete) {
         form.submit();
       }
     });
 });

</script>

@endpush

{{-- @section('kategori')
@foreach ($kategori as $satukategori)
  <li class="nav-item">
    <a href="/kategori/{{$satukategori->id}}" class="nav-link">
      {{$satukategori->nama}}
    </a>
  </li>
@endforeach

@endsection --}}


@push('scripts')

@if(Session::has('success'))
  <script type="text/javascript">
  function message() {
    Swal.fire({
      title: "Berhasil!",
      text: "Pertanyaan/Diskusi telah diedit!",
      icon: "success",
      confirmButtonText: "Cool",
  }) };

  window.onload = message;
 </script>
@endif
@endpush