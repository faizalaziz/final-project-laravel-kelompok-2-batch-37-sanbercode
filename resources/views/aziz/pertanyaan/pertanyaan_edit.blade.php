@extends('halamantanpaforum')
@section('title')
    Edit Pertanyaan
@endsection
@section('isihalamankosong')
    <div class="card card-primary card-outline">
        <!-- /.card-header -->

        <div class="card-header">
            <h3 class="card-title">Edit Pertanyaan</h3>
        </div>
         
        <!-- /.card-body -->
        <form action="/pertanyaan/{{$pertanyaan_edit->id}}" method="post" enctype="multipart/form-data" >
        <div class="card-body">
                @method('PUT')
                @csrf  
                <div class="form-group">
                    <label for="kategori_id">Pilih kategori</label>
                    <select name="kategori_id" id="kategori_id" class="form-control">
                        @foreach ($kategori as $satukategori)
                            <option value="{{$satukategori->id}}">{{$satukategori->nama}}</option>
                        @endforeach
                    </select>
                    @error('kategori_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                

                <div class="form-group">
                    <label for="deskripsi">Tulis pertanyaanmu!</label>
                    <textarea id="deskripsi" name="deskripsi" class="form-control" style="height: 300px">{{$pertanyaan_edit->deskripsi}}</textarea>
                    @error('deskripsi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                
                </div>

                <div class="form-group">
                    <div class="btn btn-default btn-file">
                        <i class="fas fa-paperclip"></i> Attachment
                        <input type="file" name="gambar" id="gambar">

                    </div>
                    <p class="help-block">Max. 32MB</p>
                </div>

        </div>
         
        <!-- /.card-footer -->
        <div class="card-footer">
            <div class="float-right">
                <button type="submit" class="btn btn-primary">Edit Pertanyaan</button>
                <a type="submit" class="btn btn-warning" href="/pertanyaan/{{$pertanyaan_edit->id}}">Batalkan Edit Pertanyaan</a>
            </div>
        </div>
        </form>
      
    </div>
    <!-- /.card -->

@endsection


@push('scriptatas')
<script src="https://cdn.tiny.cloud/1/g4fsy22erv44flvoxov67ikjdrtlpvfo6gw0yffuunsg9vp7/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: '#deskripsi',
        menubar:false,
        statusbar: false,
    });
  </script>
@endpush
@push('scripts')

@endpush

