<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/sign-in/" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {{-- <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet" /> --}}

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/signin.css') }}" rel="stylesheet" />
</head>

<body class="text-center">
    <form class="form-signin" action="/register" method="POST">
        @csrf
        <h1 class="h3 mb-5 font-weight-normal">Registration Form</h1>
        <label for="no_induk" class="">No Induk<span class="text-primary">*</span></label>
        <input type="text" name="no_induk" id="no_induk" class="form-control my-1" placeholder="Input NIS/No.Ijazah"
            required maxlength="10" autofocus />

        <label for="username" class="">Nama Lengkap <span class="text-primary">*</span></label>
        <input type="text" name="username" id="username" class="form-control my-1" placeholder="Nama Lengkap" required
            maxlength="100" autofocus />

        <label for="biodata" class="">Biodata</label>
        <textarea name="biodata" id="biodata" class="form-control my-1" placeholder="Biodata" cols="30" rows="10"></textarea>

        <label for="umur" class="">Umur</label>
        <input type="number" name="umur" id="umur" class="form-control my-1" placeholder="Umur" required
            maxlength="10" autofocus />

        <label for="alamat" class="">Alamat</label>
        <textarea name="alamat" id="alamat" class="form-control my-1" placeholder="Alamat" cols="30" rows="10"></textarea>

        {{-- <div class="form-group">
            <label for="jabatan_id">Jabatan</label>
            <select name="jabatan_id" class="form-control" id="jabatan_id">
              <option value="">---Pilih Jabatan---</option>
              @forelse ($jabatan as $item)
                  <option value="{{$item->id}}" selected>{{$item->status}}</option>  
              @empty
                  Tidak ada kategori
              @endforelse
            </select>
          </div>
          @error('jabatan_id')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror --}}

        <label for="email" class="">Email address <span class="text-primary">*</span></label>
        <input type="email" name="email" id="email" class="form-control my-1 @error('email') is-invalid @enderror"
            placeholder="Email address" required autofocus />
        @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror

        <label for="password" class="">Password</label>
        <input type="password" name="password" id="password"
            class="form-control @error('password') is-invalid @enderror" placeholder="Password" maxlength="8"
            required />
        @error('password')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
        <p><span class="text-primary">*</span><span class="text-danger">Tidak bisa diganti, tolong cek data anda kembali sebelum register</span></p>
        <button class="btn btn-lg btn-primary btn-block" type="submit">
            Register
        </button>
        <small class="d-block text-center my-3">
            Already Registered? <a href="/login">Login</a>
        </small>

    </form>
    {{-- <script>
        const nis = document.querySelector('#nis');
        const username = document.querySelector('#username');

        nis.addEventListener('change', function() {
            fetch('/nis/checkNis?no_induk=' + nis.value)
                .then(response => response.json())
                // .then(response => console.log(response));
                .then(data => username.value = data.username)
        });
    </script> --}}
</body>

</html>
