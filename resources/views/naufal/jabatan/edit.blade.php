@extends('halamantanpaforum')
@section('title')
    Edit Jabatan
@endsection
@section('isihalamankosong')
<form action="/jabatan/{{$jabatan->id}}" method="POST" class="form">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="status">Edit Status Keanggotaan</label>
      <input name="status" type="text" value="{{old('status', $jabatan->status)}}" class="form-control" id="status">
    </div>
    @error('status')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror
    <button type="submit" class="btn btn-primary">Update</button>
    <a href="/jabatan" class="btn btn-danger">Batalkan Edit</a>

  </form>


@endsection