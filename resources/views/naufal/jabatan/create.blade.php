@extends('halamantanpaforum')
@section('title')
    Buat Jabatan
@endsection
@section('isihalamankosong')

    <div class="card card-primary card-outline">
        <!-- /.card-header -->
        <div class="float-left card-header">
            <a href="/jabatan" class="btn btn-primary">< Kembali</a>
        </div>
        <div class="card-header">
            <h3 class="card-title">Buat Jabatan Baru</h3>
        </div>
         
        <!-- /.card-body -->
        <form action="/jabatan" method="post" >
        <div class="card-body">
            @csrf
            <div class="form-group">
              <label for="status">Status Keanggotaan</label>
              <input name="status" type="text" value="{{old('status')}}" class="form-control" id="status">
            </div>
            @error('status')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      
    </div>
    <!-- /.card -->

@endsection


{{-- @section('kategori')
@foreach ($kategori as $satukategori)
  <li class="nav-item">
    <a href="/categories/{{$satukategori->id}}" class="nav-link">
      {{$satukategori->nama}}
    </a>
  </li>
@endforeach

@endsection --}}