@extends('halamantanpaforum')
@section('title')
    Daftar Jabatan
@endsection
@section('isihalamankosong')

<div class="card card-primary card-outline">
    <!-- /.card-header -->
    <div class="float-left card-header">
        <a href="/pertanyaan" class="btn btn-primary">< Kembali</a>
    </div>
    <div class="card-header">
        <h1 class="card-title"><b>Jabatan</b></h1>
    </div>

    @auth
    <a href="/jabatan/create" class="btn btn-info btn-sm my-3">Tambah jabatan</a>
    @endauth

    <table class="table">
        <thead class="thead-light">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($jabatan as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->status}}</td>
                    <td>
                        <form action="/jabatan/{{$value->id}}" method="POST">
                            @csrf
                            <a href="/jabatan/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/jabatan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
</div>

@endsection