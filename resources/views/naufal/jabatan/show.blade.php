@extends('halamantanpaforum')
@section('title')
    Jabatan | {{$jabatan->status}}
@endsection
@section('isihalamankosong')

<h1 class='text-primary'>List User {{$jabatan->status}}</h1>


<div class="card card-primary card-outline">
    <!-- /.card-header -->
            <div class="card-header">
                <h3 class="card-title">List</h3>
            
                <div class="card-tools">
                  <div class="input-group input-group-sm">
                    <input type="text" class="form-control" placeholder="Cari User" id="searchforum" onkeyup="myFunction()">
                    <div class="input-group-append">
                      <div class="btn btn-primary">
                        <i class="fas fa-search"></i>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive mailbox-messages p-3">
                <table class="table table-hover table-striped" id="myTable">
                
                <thead class="">
                  <th class="">Nama Lengkap</th>
                  <th class="">No Induk</th>
                </thead>
                <tbody>
                
                @forelse ($jabatan->profile as $satuprofile)
                   <tr>
                    <td width="50%"><a class="text-primary" href="/profile/{{$satuprofile->user->id}}">{{$satuprofile->user->username}}</a></td>
                    <td width="50%">{{$satuprofile->user->no_induk}}</td>
                   </tr>
                @empty
                  <tr>
                    <td>Tidak ada User</td>
                  </tr>
                @endforelse
                </tbody>
                </table>

            </div>
    </div>
</div>

<a href="/jabatan" class="btn btn-secondary btn-md">Kembali</a>

@endsection
