@extends('halamantanpaforum')
@section('title')
    Profile
@endsection
@section('isihalamankosong')

<div class="card card-primary card-outline">
    <!-- /.card-header -->
            <div class="card-header">
                {{-- <a class="btn btn-primary card-title" href="/pertanyaan"><i class="fas fa-chevron-left"></i>Kembali</a> --}}
                <h3 class="card-title">User Profile</h3>
            </div>
            <div class="card-body p-0">

            <div class="table-responsive mailbox-messages p-3">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-sm" >
                            <div class="text-center ">
                                <h3><b>{{$profile->user->username}}</b></h3>
                                <p>{{$profile->user->email}}</p>
                                <p class="m-0">No. Induk : {{$profile->user->no_induk}}</p>
                            </div>
                        </div>
                        <div class="col-sm border-left text-center">
                            <h5><b><u>About me </u></b></h5>
                            <p>{{$profile->biodata}}</p>
                            <hr>
                            <div class="row">
                                <div class="col-sm">
                                    @if (empty($profile->jabatan_id))
                                        <p><i class="fa fa-user"></i> Belum diisi</p>
                                    @else
                                        <p><i class="fa fa-user"></i>  {{$profile->jabatan->status}}</p>
                                    @endif
                                </div>
                                <div class="col-sm">
                                    <p><i class="fa fa-birthday-cake" aria-hidden="true"></i> {{$profile->umur}}</p>
                                </div>
                            </div>
                            <p><i class="fas fa-home"></i>{{"   ".$profile->alamat}}</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <a href="/profile/{{$profile->id}}/edit" class="btn btn-info btn-sm">Lengkapi Atau Edit Profil</a>
            <a href="/pertanyaan" class="btn btn-primary btn-sm my-2">Kembali ke Forum</a>

</div>



    
@endsection