@extends('halamantanpaforum')
@section('title')
    Edit Profile
@endsection
@section('isihalamankosong')
    <div class="card card-primary card-outline">
        <!-- /.card-header -->
        <div class="float-left card-header">
            <a href="/profile" class="btn btn-primary">< Kembali</a>
        </div>
        <div class="card-header">
            <h3 class="card-title">Edit Profil</h3>
        </div>
         
        <!-- /.card-body -->
        <form action="/profile/{{$profile->id}}" method="post">
        <div class="card-body">
                @csrf 
                @method('PUT')
                <div class="form-group">
                    <label for="user_not_submit">User</label>
                    <input type="text" name="user_not_submit" id="user_not_submit" value="{{$profile->user->username}}" class="form-control my-3" readonly>
                    <input type="hidden" name="user_id" value="{{$profile->user->id}}">
                    @error('user_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" value="{{$profile->user->email}}" class="form-control my-3" readonly>
                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="biodata">Biodata</label>
                    <textarea name="biodata" id="biodata" class="form-control my-3" cols="30" rows="10">{{$profile->biodata}}</textarea>
                    @error('biodata')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                
                </div>

                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" name="umur" value="{{$profile->umur}}" id="umur" class="form-control my-3">
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" id="alamat" class="form-control my-3" cols="30" rows="10">{{$profile->alamat}}</textarea>
                    @error('alamat')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="jabatan_id">Jabatan</label>
                    <select name="jabatan_id" id="jabatan_id" class="form-control my-3">
                        @foreach ($jabatan as $item)
                            <option value="{{$item->id}}">{{$item->status}}</option>
                        @endforeach
                    </select>
                    @error('jabatan_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                {{-- <div class="form-group">
                    <label for="kategori_id">Pilih kategori</label>
                    <select name="kategori_id" id="kategori_id" class="form-control">
                        @foreach ($kategori as $satukategori)
                            <option value="{{$satukategori->id}}">{{$satukategori->nama}}</option>
                        @endforeach
                    </select>
                    @error('kategori_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="deskripsi">Tulis pertanyaanmu!</label>
                    <textarea id="deskripsi" name="deskripsi" class="form-control" style="height: 300px"></textarea>
                    @error('deskripsi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                
                </div> --}}


        </div>
         
        <!-- /.card-footer -->
        <div class="card-footer">
            <div class="float-right">
            <button type="submit" class="btn btn-primary">Perbaharui Profil</button>
            </div>
        </div>
        </form>
      
    </div>
    <!-- /.card -->

@endsection


@section('kategori')
{{-- @foreach ($kategori as $satukategori)
  <li class="nav-item">
    <a href="/categories/{{$satukategori->id}}" class="nav-link">
      {{$satukategori->nama}}
    </a>
  </li>
@endforeach --}}

@endsection

@push('scriptatas')
<script src="https://cdn.tiny.cloud/1/g4fsy22erv44flvoxov67ikjdrtlpvfo6gw0yffuunsg9vp7/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: '#deskripsi',
        menubar:false,
        statusbar: false,
    });
  </script>
@endpush
@push('scripts')

@endpush