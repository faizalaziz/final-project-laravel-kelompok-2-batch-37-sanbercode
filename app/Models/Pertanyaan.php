<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;
    protected $table = 'pertanyaan';
    protected $fillable = [
        'deskripsi','gambar','kategoris_id','user_id'
    ];

    public function komentar()
    {
        return $this->hasMany(Komentar::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function kategoris()
    {
        return $this->belongsTo(Kategori::class,);
    }
}
