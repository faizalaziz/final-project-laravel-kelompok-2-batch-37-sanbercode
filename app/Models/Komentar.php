<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    use HasFactory;
    protected $table = 'komentar';
    protected $fillable = [
        'komentar','pertanyaan_id','user_id'
    ];
    public function pertanyaan()
    {
        return $this->belongsTo(Pertanyaan::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
