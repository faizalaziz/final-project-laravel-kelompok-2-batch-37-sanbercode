<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Http\Request;
use App\Models\Pertanyaan;
use App\Models\Komentar;
use App\Models\Kategori;
use App\Models\User;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::get();
        $pertanyaan = Pertanyaan::orderBy('created_at','desc')->get();
        return view('aziz.pertanyaan.pertanyaan',['kumpulanpertanyaan' => $pertanyaan,'kategori'=>$kategori]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('aziz.pertanyaan.pertanyaan_create',['kategori'=>$kategori]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pertanyaan = new Pertanyaan;
        $request->validate([
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg|max:4096',
            'kategori_id' => 'required'
        ]);
        if ($request->has('gambar')) {
            $fileGambar = time() . '.' . $request->gambar->extension();  
            $request->gambar->move(public_path('uploads'), $fileGambar);
            $pertanyaan->gambar = $fileGambar;
        }
        
        

        $pertanyaan->deskripsi = $request->deskripsi;
        $pertanyaan->kategoris_id = $request->kategori_id;
        $pertanyaan->user_id = Auth::id();
        $pertanyaan->save();


        return redirect('/pertanyaan')->withSuccess('Success message');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan_id = Pertanyaan::find($id);
        $kategori = Kategori::get();
        $user = Auth::id();
        return view('aziz.pertanyaan.pertanyaan_id',['pertanyaan_id'=>$pertanyaan_id,'kategori'=>$kategori,'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::get();
        $pertanyaan_edit = Pertanyaan::find($id);
        if ($pertanyaan_edit->user_id == Auth::id()) {
            return view('aziz.pertanyaan.pertanyaan_edit',['pertanyaan_edit'=>$pertanyaan_edit,'kategori'=>$kategori]); 
        }else {
            $kategori = Kategori::get();
            $pertanyaan = Pertanyaan::orderBy('created_at','desc')->get();
            return view('aziz.pertanyaan.pertanyaan',['kumpulanpertanyaan' => $pertanyaan,'kategori'=>$kategori]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pertanyaan_edit = Pertanyaan::find($id);

        $request->validate([
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg|max:2048',
            'kategori_id' => 'required'
        ]);

        if ($request->has('gambar')) {
            $path = 'uploads/';
            File::delete($path . $pertanyaan_edit->gambar);

            $fileGambar = time().'.'.$request->gambar->extension();  
            $request->gambar->move(public_path('uploads'), $fileGambar);

            $pertanyaan_edit->gambar = $fileGambar;
        }

        $pertanyaan_edit->deskripsi = $request->deskripsi;
        $pertanyaan_edit->kategoris_id = $request->kategori_id;
        $pertanyaan_edit->update();

        
        return redirect('/pertanyaan/'. $pertanyaan_edit->id)->withSuccess('Success message');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan_hapus = Pertanyaan::find($id);
        if (!empty($pertanyaan_hapus->gambar)) {
            $path = 'uploads/';
            File::delete($path . $pertanyaan_hapus->gambar);
        }
        $pertanyaan_hapus->delete();
        return redirect('/pertanyaan');
    }
}
