<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Http\Request;
use App\Models\Pertanyaan;
use App\Models\Komentar;
use App\Models\Kategori;
use App\Models\User;

class PopularController extends Controller
{
    public function index()
    {
        $kategori = Kategori::get();
        $pertanyaan = Pertanyaan::withCount('komentar')->orderBy('komentar_count','desc')->get();
        return view('aziz.pertanyaan.pertanyaan',['kumpulanpertanyaan' => $pertanyaan,'kategori'=>$kategori]);
    }
}
