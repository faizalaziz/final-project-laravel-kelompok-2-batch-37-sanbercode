<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;


use App\Models\Pertanyaan;
use App\Models\Komentar;
use App\Models\Kategori;
use App\Models\User;

use Illuminate\Http\Request;

class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $komentar = new Komentar;

        $komentar->komentar = $request->komentar;
        $komentar->user_id = Auth::id();
        $komentar->pertanyaan_id = $id;
        $komentar->save();

        return redirect('/pertanyaan/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $komentar_edit = Komentar::find($id);

        $komentar_edit->komentar = $request->komentar_edit;
        $komentar_edit->user_id = Auth::id();
        $komentar_edit->update();

        return redirect('/pertanyaan/' . $komentar_edit->pertanyaan_id . "#komentar_" . $komentar_edit->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $komentar_hapus = Komentar::find($id);
        $id_pertanyaan_hapus = $komentar_hapus->pertanyaan_id;
        $komentar_hapus->delete();
        return redirect('pertanyaan/' . $id_pertanyaan_hapus);
    }
}
