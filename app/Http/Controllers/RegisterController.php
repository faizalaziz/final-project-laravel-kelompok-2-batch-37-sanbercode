<?php

namespace App\Http\Controllers;

use App\Models\Profil;
use App\Models\Profile;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Jabatan;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register.index');
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:8',
            'no_induk' => 'required|unique:users',
            'username' => 'required'
        ]);
        $validatedData['password'] = Hash::make($validatedData['password']);
        // $validatedData['no_induk'] = '1212020021';
        $user = User::create($validatedData);
        Profile::create([
            'biodata' => $request->biodata,
            'email' => $user->email,
            'umur' => $request->umur,
            'alamat' => $request->alamat,
            'user_id' => $user->id,
        ]);
        $request->session()->flash('success', 'Registration successfull! ');

        return redirect('/login');
        // $test = User::firstWhere('email', $request->email);
        // return $test;
    }

}
