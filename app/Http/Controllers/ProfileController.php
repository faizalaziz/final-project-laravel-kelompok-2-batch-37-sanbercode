<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;
use App\Models\Jabatan;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser = Auth::id();
        $profile = Profile::where('user_id', $iduser)->first();
        $jabatan = Jabatan::all();

        return view('naufal.profile.index',['profile' => $profile], ['jabatan' => $jabatan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $iduser = Auth::id();
        // $user = User::where('id', $iduser)->first();
        // $jabatan = Jabatan::all();
        // return view ('naufal.profile.index', ['user' => $user], ['jabatan' => $jabatan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'biodata' => 'required',
        //     'umur' => 'required',
        //     'email' => 'required',
        //     'alamat' => 'required',
        //     'jabatan_id' => 'required',
        // ]);

        // $profile = new Profile;
        

        // $profile->biodata = $request->biodata;
        // $profile->umur = $request->umur;
        // $profile->email = $request->email;
        // $profile->alamat = $request->alamat;
        // $profile->user_id = $request->user_id;
        // $profile->jabatan_id = $request->jabatan_id;

        // $profile->save();

        // return redirect ('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        $jabatan = Jabatan::all();

        return view('naufal.profile.indexkhususviewer',['profile' => $profile], ['jabatan' => $jabatan]);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $iduser = Auth::id();
        $profile = Profile::where('user_id', $iduser)->first();
        $jabatan = Jabatan::all();
        return view ('naufal.profile.edit', ['profile' => $profile], ['jabatan' => $jabatan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'biodata' => 'required',
            'email' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
            'jabatan_id' => 'required',
            
        ]);

        $profile = Profile::find($id);
        $jabatan = Jabatan::find($id);

        $profile->biodata = $request->biodata;
        $profile->email = $request->email;
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->jabatan_id = $request->jabatan_id;

        $profile->save();

        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
